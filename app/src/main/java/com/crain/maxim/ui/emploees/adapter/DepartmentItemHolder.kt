package com.crain.maxim.ui.emploees.adapter

import android.graphics.drawable.Drawable
import android.view.View
import androidx.core.content.ContextCompat
import com.crain.maxim.R
import kotlinx.android.synthetic.main.item_department.view.*

class DepartmentItemHolder(itemView: View, private val mListener: IDepartmentItemHolderListener) :
    BaseEmployeeItemHolder(itemView) {

    interface IDepartmentItemHolderListener {
        fun onDepartmentClick(position: Int)
    }

    companion object {
        val layoutId = R.layout.item_department
    }

    init {
        itemView.setOnClickListener { mListener.onDepartmentClick(adapterPosition) }
    }

    override fun bindTo(listItem: ListItem) {
        super.bindTo(listItem)

        itemView.itemDepartmentImage.setImageDrawable(getImage(listItem.hasChildren))
        itemView.itemDepartmentName.text = listItem.staffItem.name
    }

    private fun getImage(hasChildren: Boolean): Drawable? {
        val drawableId = if (hasChildren) R.drawable.ic_group else R.drawable.ic_group_empty
        return ContextCompat.getDrawable(itemView.context, drawableId)
    }
}