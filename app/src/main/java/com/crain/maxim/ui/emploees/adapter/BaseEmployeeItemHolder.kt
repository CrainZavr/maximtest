package com.crain.maxim.ui.emploees.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.crain.maxim.R

abstract class BaseEmployeeItemHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    open fun bindTo(listItem: ListItem) {
        itemView.setPadding(
            (listItem.level + 1) * itemView.context.resources.getDimension(R.dimen.default_padding).toInt(),
            itemView.paddingTop,
            itemView.paddingRight,
            itemView.paddingBottom
        )
    }
}