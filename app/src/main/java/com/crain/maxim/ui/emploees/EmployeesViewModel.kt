package com.crain.maxim.ui.emploees

import androidx.lifecycle.LiveData
import com.crain.maxim.App
import com.crain.maxim.services.repository.IRepository
import com.crain.maxim.services.repository.models.Department
import com.crain.maxim.types.SingleLiveEvent
import com.crain.maxim.types.StateLiveData
import com.crain.maxim.ui.BaseViewModel
import kotlinx.coroutines.launch

class EmployeesViewModel(
    private val mRepository: IRepository = App.serviceProvider.repository
) : BaseViewModel() {
    private val mDepartmentsData: StateLiveData<Department> = StateLiveData()
    val errorData: SingleLiveEvent<String> = SingleLiveEvent()

    fun getDepartmentsData(): LiveData<Department> {
        if (mDepartmentsData.checkNotCalculatedAndSet())
            loadDepartments()

        return mDepartmentsData
    }

    private fun loadDepartments() {
        launch {
            val getDepartmentsResult = mRepository.getAllDepartments()
            if (getDepartmentsResult.isSuccess) {
                mDepartmentsData.postValue(getDepartmentsResult.data)
            } else {
                errorData.postValue(getDepartmentsResult.error)
            }
        }
    }
}