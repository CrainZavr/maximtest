package com.crain.maxim.ui.login.signin


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.crain.maxim.R
import com.crain.maxim.types.AppResult
import com.crain.maxim.ui.BaseFragment
import com.crain.maxim.ui.MainActivity
import kotlinx.android.synthetic.main.fragment_sign_in.*

class SignInFragment : BaseFragment() {

    private lateinit var mViewModel: SignInViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel = ViewModelProviders.of(this).get(SignInViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_in, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        signInButton.setOnClickListener { onSignInButtonClick() }

        mViewModel.signInResultData.observe(this, Observer { onSignInResultDataObserve(it) })
        mViewModel.verifyNameResultData.observe(this, Observer { signInNameLayout.error = it.error })
        mViewModel.verifyPasswordResultData.observe(this, Observer { signInPasswordLayout.error = it.error })
    }

    private fun onSignInButtonClick() {
        signInError.text = ""
        mViewModel.signIn(signInNameEdit.text.toString(), signInPasswordEdit.text.toString())
    }

    private fun onSignInResultDataObserve(appResult: AppResult<Boolean>) {
        if (appResult.isSuccess) {
            activity.startActivity(Intent(context, MainActivity::class.java))
            activity.finish()
        } else {
            signInError.text = appResult.error
        }
    }
}
