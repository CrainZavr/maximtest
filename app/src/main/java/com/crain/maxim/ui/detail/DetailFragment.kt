package com.crain.maxim.ui.detail


import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.crain.maxim.R
import com.crain.maxim.services.repository.models.Employee
import com.crain.maxim.ui.BaseFragment
import kotlinx.android.synthetic.main.fragment_detail.*


class DetailFragment : BaseFragment() {

    private lateinit var mViewModel: DetailViewModel

    companion object {
        fun create(employee: Employee): DetailFragment {
            val fragment = DetailFragment()
            val bundle = Bundle()
            bundle.putSerializable(KEY_EMPLOYEE_ID, employee)
            fragment.arguments = bundle

            return fragment
        }

        private const val KEY_EMPLOYEE_ID = "EMPLOYEE_ID"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel = ViewModelProviders.of(this).get(DetailViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val employee = getEmployee() ?: return
        mViewModel.getEmployeePhoto(employee.id).observe(this, Observer { onGetEmployeePhotoObserve(it) })

        val phone = employee.phone
        val email = employee.email

        detailPhoneContainer.setOnClickListener { onPhoneClick(phone) }
        detailEmailContainer.setOnClickListener { onEmailClick(email) }

        detailName.text = employee.name
        detailTitle.text = employee.title
        detailPhone.text = phone
        detailEmail.text = email
    }

    private fun getEmployee(): Employee? {
        return arguments?.getSerializable(KEY_EMPLOYEE_ID) as? Employee
    }

    private fun onGetEmployeePhotoObserve(bitmap: Bitmap) {
        detailPhoto.setImageBitmap(bitmap)
    }

    private fun onPhoneClick(phone: String) {
        if (phone.isBlank()) return
        val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
        startActivity(Intent.createChooser(intent, null));
    }

    private fun onEmailClick(email: String) {
        if (email.isBlank()) return
        val intent = Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", email, null))
        startActivity(Intent.createChooser(intent, null));
    }
}
