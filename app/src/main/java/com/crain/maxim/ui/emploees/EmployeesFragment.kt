package com.crain.maxim.ui.emploees


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.crain.maxim.R
import com.crain.maxim.services.repository.models.Department
import com.crain.maxim.services.repository.models.Employee
import com.crain.maxim.ui.BaseFragment
import com.crain.maxim.ui.detail.DetailFragment
import com.crain.maxim.ui.emploees.adapter.EmployeeListAdapter
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_employees.*

class EmployeesFragment : BaseFragment(), EmployeeListAdapter.IEmployeeListAdapterListener {

    private lateinit var mViewModel: EmployeesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel = ViewModelProviders.of(this).get(EmployeesViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_employees, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mViewModel.getDepartmentsData().observe(this, Observer { onGetDepartmentsObserve(it) })
        mViewModel.errorData.observe(this, Observer { Snackbar.make(view, it, Snackbar.LENGTH_LONG).show() })
    }

    private fun onGetDepartmentsObserve(department: Department) {
        employeeList.adapter = EmployeeListAdapter(department, this)
        employeeList.layoutManager = LinearLayoutManager(context)
    }

    override fun onEmployeeClick(employee: Employee) {
        activity.addFragment(DetailFragment.create(employee))
    }
}
