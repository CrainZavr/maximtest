package com.crain.maxim.ui.emploees.adapter

import android.view.View
import com.crain.maxim.R
import com.crain.maxim.services.repository.models.Employee
import kotlinx.android.synthetic.main.item_employee.view.*

class EmployeeItemHolder(itemView: View, private val mListener: IEmployeeItemHolderListener) :
    BaseEmployeeItemHolder(itemView) {

    interface IEmployeeItemHolderListener {
        fun onEmployeeClick(position: Int)
    }

    companion object {
        val layoutId = R.layout.item_employee
    }

    init {
        itemView.setOnClickListener { mListener.onEmployeeClick(adapterPosition) }
    }

    override fun bindTo(listItem: ListItem) {
        super.bindTo(listItem)

        itemView.itemEmployeeName.text = listItem.staffItem.name
        itemView.itemEmployeeTitle.text = if (listItem.staffItem is Employee) listItem.staffItem.title else ""
    }
}