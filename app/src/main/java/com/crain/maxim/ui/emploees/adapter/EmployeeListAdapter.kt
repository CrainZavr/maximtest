package com.crain.maxim.ui.emploees.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.crain.maxim.services.repository.models.Department
import com.crain.maxim.services.repository.models.Employee

class EmployeeListAdapter(
    private val mDepartmentsTree: Department,
    private val mListener: IEmployeeListAdapterListener
) : RecyclerView.Adapter<BaseEmployeeItemHolder>(),
    DepartmentItemHolder.IDepartmentItemHolderListener, EmployeeItemHolder.IEmployeeItemHolderListener {

    interface IEmployeeListAdapterListener {
        fun onEmployeeClick(employee: Employee)
    }

    private val mList: MutableList<ListItem> = ArrayList()

    init {
        expandLevel(-1, -1, mDepartmentsTree.departments, mDepartmentsTree.employees)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseEmployeeItemHolder {
        return when (viewType) {
            ListItemType.Department.ordinal -> {
                val view = LayoutInflater.from(parent.context).inflate(DepartmentItemHolder.layoutId, parent, false)
                DepartmentItemHolder(view, this)
            }
            else -> {
                val view = LayoutInflater.from(parent.context).inflate(EmployeeItemHolder.layoutId, parent, false)
                EmployeeItemHolder(view, this)
            }
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onBindViewHolder(holder: BaseEmployeeItemHolder, position: Int) {
        val item = mList[position]
        holder.bindTo(item)
    }

    override fun getItemViewType(position: Int): Int {
        return mList[position].type.ordinal
    }

    private fun expandLevel(
        currentItemPosition: Int,
        currentItemLevel: Int,
        departments: Array<Department>,
        employees: Array<Employee>
    ) {
        val newLevel = currentItemLevel + 1
        val newItems =
            departments.map { ListItem(it, newLevel) } +
                    employees.map { ListItem(it, newLevel) }
        val startNewItemsPosition = currentItemPosition + 1
        mList.addAll(startNewItemsPosition, newItems)

        notifyItemRangeInserted(startNewItemsPosition, newItems.size)
    }

    private fun collapseLevel(currentItemPosition: Int, currentItemLevel: Int) {
        val removeList = ArrayList<ListItem>()
        val startRemoveItemsPosition = currentItemPosition + 1
        var i = startRemoveItemsPosition
        while (i < mList.size && currentItemLevel < mList[i].level) {
            removeList.add(mList[i])
            i++
        }
        if (removeList.isNotEmpty()) {
            mList.removeAll(removeList)

            notifyItemRangeRemoved(startRemoveItemsPosition, removeList.size)
        }
    }

    override fun onEmployeeClick(position: Int) {
        val employee = mList[position].staffItem as? Employee ?: return
        mListener.onEmployeeClick(employee)
    }

    override fun onDepartmentClick(position: Int) {
        val item = mList[position]
        val needExpand = position == mList.size - 1 || mList[position + 1].level <= item.level
        if (needExpand) {
            val department = item.staffItem as? Department ?: return
            expandLevel(position, item.level, department.departments, department.employees)
        } else {
            collapseLevel(position, item.level)
        }
    }
}