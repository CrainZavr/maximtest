package com.crain.maxim.ui

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.crain.maxim.App
import com.crain.maxim.R
import com.crain.maxim.ui.emploees.EmployeesFragment
import com.crain.maxim.ui.login.LoginActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    private val mCurrentUser = App.serviceProvider.currentUser

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initToolbar()

        if (savedInstanceState == null)
            replaceFragment(EmployeesFragment(), false)

        supportFragmentManager.addOnBackStackChangedListener {
            syncDisplayHomeAsUp()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_settings -> {
                onLogoutClick()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override val fragmentContainerId: Int = R.id.fragmentContainer

    private fun onLogoutClick() {
        mCurrentUser.logout()
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        syncDisplayHomeAsUp()
    }

    private fun syncDisplayHomeAsUp() {
        supportActionBar?.setDisplayHomeAsUpEnabled(supportFragmentManager.backStackEntryCount > 0)
    }
}
