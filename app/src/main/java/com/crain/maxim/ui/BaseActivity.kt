package com.crain.maxim.ui

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction

abstract class BaseActivity : AppCompatActivity() {

    abstract val fragmentContainerId: Int

    fun replaceFragment(frag: BaseFragment, isToBackStack: Boolean = true) {
        setFragment(frag, isToBackStack, false)
    }

    fun addFragment(frag: BaseFragment, isToBackStack: Boolean = true) {
        setFragment(frag, isToBackStack, true)
    }

    fun setFragment(frag: BaseFragment, isToBackStack: Boolean, isAdd: Boolean) {
        val transaction = supportFragmentManager.beginTransaction()
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)

        if (isAdd) {
            transaction.add(fragmentContainerId, frag)
        } else {
            transaction.replace(fragmentContainerId, frag)
        }

        if (isToBackStack) {
            transaction.addToBackStack(null)
        }

        transaction.commit()
    }
}