package com.crain.maxim.ui.emploees.adapter

import com.crain.maxim.services.repository.models.Department
import com.crain.maxim.services.repository.models.StaffItem

class ListItem(
    val staffItem: StaffItem,
    val level: Int
) {
    val type = if (staffItem is Department) ListItemType.Department else ListItemType.Employee
    val hasChildren =
        staffItem is Department && (staffItem.departments.isNotEmpty() || staffItem.employees.isNotEmpty())
}

enum class ListItemType {
    Department,
    Employee
}



