package com.crain.maxim.ui

import android.content.Context
import androidx.fragment.app.Fragment
import java.lang.Exception

abstract class BaseFragment : Fragment() {

    lateinit var activity: BaseActivity private set

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context !is BaseActivity)
            throw Exception("Invalid activity type")

        activity = context
    }
}