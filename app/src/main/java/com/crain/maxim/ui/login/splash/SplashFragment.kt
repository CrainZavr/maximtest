package com.crain.maxim.ui.login.splash


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.crain.maxim.ui.BaseFragment
import com.crain.maxim.R
import com.crain.maxim.ui.MainActivity
import com.crain.maxim.ui.emploees.EmployeesFragment
import com.crain.maxim.ui.login.signin.SignInFragment

class SplashFragment : BaseFragment() {

    private lateinit var mViewModel: SplashViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel = ViewModelProviders.of(this).get(SplashViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mViewModel.checkNeedLogin().observe(this, Observer { needLogin ->
            if (needLogin) {
                activity.replaceFragment(SignInFragment(), false)
            }
            else {
                activity.startActivity(Intent(context, MainActivity::class.java))
                activity.finish()
            }
        })
    }
}
