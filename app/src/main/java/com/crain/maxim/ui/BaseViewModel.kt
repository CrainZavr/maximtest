package com.crain.maxim.ui

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

open class BaseViewModel : ViewModel(), CoroutineScope {

    private val mJob: Job = Job()

    override val coroutineContext: CoroutineContext = mJob

    override fun onCleared() {
        super.onCleared()

        mJob.cancel()
    }
}