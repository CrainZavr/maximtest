package com.crain.maxim.ui.login

import android.os.Bundle
import com.crain.maxim.R
import com.crain.maxim.ui.BaseActivity
import com.crain.maxim.ui.login.splash.SplashFragment

class LoginActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        if (savedInstanceState == null)
            replaceFragment(SplashFragment(), false)
    }

    override val fragmentContainerId = R.id.loginFragmentContainer
}
