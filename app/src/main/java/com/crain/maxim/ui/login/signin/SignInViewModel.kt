package com.crain.maxim.ui.login.signin

import androidx.lifecycle.MutableLiveData
import com.crain.maxim.App
import com.crain.maxim.services.api.IApiService
import com.crain.maxim.types.AppResult
import com.crain.maxim.types.CurrentUser
import com.crain.maxim.ui.BaseViewModel
import kotlinx.coroutines.launch

class SignInViewModel(
    private val mApiService: IApiService = App.serviceProvider.apiService,
    private val mCurrentUser: CurrentUser = App.serviceProvider.currentUser
) : BaseViewModel() {
    val signInResultData: MutableLiveData<AppResult<Boolean>> = MutableLiveData()
    val verifyNameResultData: MutableLiveData<AppResult<Boolean>> = MutableLiveData()
    val verifyPasswordResultData: MutableLiveData<AppResult<Boolean>> = MutableLiveData()


    fun signIn(name: String, password: String) {
        launch {
            if (!verifyCredentials(name, password))
                return@launch

            val loginResult = mApiService.login(name, password)
            if (loginResult.isSuccess) {
                mCurrentUser.login(name, password)
                signInResultData.postValue(AppResult(true))
            } else {
                signInResultData.postValue(AppResult(loginResult.error))
            }
        }
    }

    private fun verifyCredentials(name: String, password: String): Boolean {
        var result = true

        fun verify(isOk: Boolean, error: String, data: MutableLiveData<AppResult<Boolean>>) {
            if (isOk) {
                data.postValue(AppResult(true))
            } else {
                data.postValue(AppResult(error))
                result = false
            }
        }

        verify(name.isNotBlank(), "Name can't be blank", verifyNameResultData)
        verify(password.isNotBlank(), "Password can't be blank", verifyPasswordResultData)

        return result
    }
}