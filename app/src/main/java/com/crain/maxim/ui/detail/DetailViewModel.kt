package com.crain.maxim.ui.detail

import android.graphics.Bitmap
import androidx.lifecycle.LiveData
import com.crain.maxim.App
import com.crain.maxim.services.repository.IRepository
import com.crain.maxim.types.StateLiveData
import com.crain.maxim.ui.BaseViewModel
import kotlinx.coroutines.launch

class DetailViewModel(private val mRepository: IRepository = App.serviceProvider.repository) : BaseViewModel() {
    private val mEmployeePhoto: StateLiveData<Bitmap> = StateLiveData()

    fun getEmployeePhoto(employeeId: String): LiveData<Bitmap> {
        if (mEmployeePhoto.checkNotCalculatedAndSet()) {
            loadEmployeeData(employeeId)
        }

        return mEmployeePhoto
    }

    private fun loadEmployeeData(employeeId: String) {
        launch {
            val byteArrayResult = mRepository.getEmployeePhoto(employeeId)
            if (byteArrayResult.isSuccess) {
                mEmployeePhoto.postValue(byteArrayResult.data)
            }
        }
    }
}