package com.crain.maxim.ui.login.splash

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.crain.maxim.App
import com.crain.maxim.types.CurrentUser
import com.crain.maxim.types.StateLiveData

class SplashViewModel(private val mCurrentUser: CurrentUser = App.serviceProvider.currentUser) : ViewModel() {
    private val mIsNeedLoginData: StateLiveData<Boolean> = StateLiveData()

    fun checkNeedLogin(): LiveData<Boolean> {
        if(mIsNeedLoginData.checkNotCalculatedAndSet()) {
            mIsNeedLoginData.postValue(!mCurrentUser.isLogged)
        }

        return mIsNeedLoginData
    }
}