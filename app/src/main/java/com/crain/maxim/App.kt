package com.crain.maxim

import android.app.Application
import com.crain.maxim.di.IServiceProvider
import com.crain.maxim.di.ServiceProvider

class App : Application() {

    companion object {
        lateinit var serviceProvider: IServiceProvider
    }

    override fun onCreate() {
        super.onCreate()

        serviceProvider = ServiceProvider(this)
    }
}