package com.crain.maxim.di

import com.crain.maxim.services.api.IApiService
import com.crain.maxim.services.repository.IRepository
import com.crain.maxim.types.CurrentUser
import retrofit2.Retrofit

interface IServiceProvider {
    val currentUser: CurrentUser
    val retrofit: Retrofit
    val apiService: IApiService
    val repository: IRepository
}