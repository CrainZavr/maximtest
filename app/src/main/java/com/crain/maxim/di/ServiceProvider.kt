package com.crain.maxim.di

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.crain.maxim.services.api.ApiService
import com.crain.maxim.services.api.IApiMethods
import com.crain.maxim.services.api.IApiService
import com.crain.maxim.services.api.RetrofitFactory
import com.crain.maxim.types.CurrentUser
import com.crain.maxim.services.preference.IPreferenceService
import com.crain.maxim.services.preference.PreferenceService
import com.crain.maxim.services.repository.IRepository
import com.crain.maxim.services.repository.Repository
import com.crain.maxim.types.Constants
import retrofit2.Retrofit

class ServiceProvider(context: Context) : IServiceProvider {

    private val mAppPref: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    private val mPrefService: IPreferenceService = PreferenceService(mAppPref)

    override val currentUser: CurrentUser by lazy { CurrentUser(mPrefService) }

    override val retrofit: Retrofit by lazy { RetrofitFactory.create(Constants.Api.BASE_URL) }

    override val apiService: IApiService by lazy {
        val apiMethods = retrofit.create(IApiMethods::class.java)
        ApiService(retrofit, apiMethods, currentUser)
    }

    override val repository: IRepository by lazy { Repository(apiService) }

}