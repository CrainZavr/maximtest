package com.crain.maxim.services.preference

import android.content.SharedPreferences

class PreferenceService(private val mAppPref: SharedPreferences) : IPreferenceService {

    companion object {
        const val CURRENT_USER_LOGIN_KEY = "CURRENT_USER_LOGIN"
        const val CURRENT_USER_PASSWORD_KEY = "CURRENT_USER_PASSWORD"
    }

    override var currentUserLogin: String
        get() = mAppPref.getString(CURRENT_USER_LOGIN_KEY, null) ?: ""
        set(value) {
            mAppPref.edit().putString(CURRENT_USER_LOGIN_KEY, value).apply()
        }

    override var currentUserPassword: String
        get() = mAppPref.getString(CURRENT_USER_PASSWORD_KEY, null) ?: ""
        set(value) {
            mAppPref.edit().putString(CURRENT_USER_PASSWORD_KEY, value).apply()
        }
}