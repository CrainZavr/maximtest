package com.crain.maxim.services.repository

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.crain.maxim.services.api.IApiService
import com.crain.maxim.services.api.models.ApiDepartment
import com.crain.maxim.services.repository.models.Department
import com.crain.maxim.types.AppResult


class Repository(private val mApiService: IApiService) : IRepository {

    override fun getAllDepartments(): AppResult<Department> {
        val apiDepartmentsResult = mApiService.getAllDepartments()
        return if (apiDepartmentsResult.isSuccess) {
            val department = ApiDepartment.convertToDepartment(apiDepartmentsResult.data)
            AppResult(department)
        } else {
            AppResult(apiDepartmentsResult.error)
        }
    }

    override fun getEmployeePhoto(employeeId: String): AppResult<Bitmap> {
        val getPhotoResult = mApiService.getEmployeePhoto(employeeId)
        return if (getPhotoResult.isSuccess) {
            val options = BitmapFactory.Options()
            options.inMutable = true
            val bitmap = BitmapFactory.decodeByteArray(getPhotoResult.data, 0, getPhotoResult.data.size, options)
            if (bitmap != null) {
                AppResult(bitmap)
            } else {
                AppResult("Bitmap decode error")
            }
        } else {
            AppResult(getPhotoResult.error)
        }
    }
}