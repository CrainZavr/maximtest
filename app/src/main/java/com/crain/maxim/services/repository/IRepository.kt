package com.crain.maxim.services.repository

import android.graphics.Bitmap
import com.crain.maxim.services.repository.models.Department
import com.crain.maxim.types.AppResult

interface IRepository {
    fun getAllDepartments(): AppResult<Department>
    fun getEmployeePhoto(employeeId: String): AppResult<Bitmap>
}