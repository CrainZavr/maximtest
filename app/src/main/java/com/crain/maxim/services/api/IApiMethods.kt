package com.crain.maxim.services.api

import com.crain.maxim.services.api.models.ApiDepartment
import com.crain.maxim.services.api.models.ApiLoginResult
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface IApiMethods {
    @GET("Hello")
    fun login(
        @Query("login") login: String,
        @Query("password") pwd: String): Call<ApiLoginResult>

    @GET("GetAll")
    fun getAllDepartments(
        @Query("login") login: String,
        @Query("password") pwd: String): Call<ApiDepartment>

    @GET("GetWPhoto")
    fun getPhoto(
        @Query("login") login: String,
        @Query("password") pwd: String,
        @Query("id") employeeId: String): Call<ResponseBody>
}