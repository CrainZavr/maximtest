package com.crain.maxim.services.repository.models

class Department (
    id: String,
    name: String,
    val departments: Array<Department>,
    val employees: Array<Employee>
) : StaffItem(id, name)