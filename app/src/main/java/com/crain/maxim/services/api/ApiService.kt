package com.crain.maxim.services.api

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.crain.maxim.services.api.models.ApiDepartment
import com.crain.maxim.services.api.models.ApiLoginResult
import com.crain.maxim.types.AppResult
import com.crain.maxim.types.Constants
import com.crain.maxim.types.CurrentUser
import com.crain.maxim.types.Logger
import retrofit2.Call
import retrofit2.Retrofit
import java.net.URL

class ApiService(
    private val mRetrofit: Retrofit,
    private val mApiMethods: IApiMethods,
    private val mCurrentUser: CurrentUser) : IApiService {

    override fun login(login: String, password: String): AppResult<ApiLoginResult> {
        return getApiResult(
            mApiMethods.login(login, password),
            whenSuccessResultProducer = {
                if(it.Success)
                    AppResult(it)
                else
                    AppResult(it.Message)
            }
        )
    }

    override fun getAllDepartments(): AppResult<ApiDepartment> {
        return getApiResult(
            mApiMethods.getAllDepartments(mCurrentUser.login, mCurrentUser.password),
            whenSuccessResultProducer = {
                if(it.Departments.isNullOrEmpty() && it.Employees.isNullOrEmpty())
                    AppResult("Error retrieving departments data")
                else
                    AppResult(it)
            }
        )
    }

    override fun getEmployeePhoto(employeeId: String): AppResult<ByteArray> {
        try {
            val response = mApiMethods.getPhoto(mCurrentUser.login, mCurrentUser.password, employeeId).execute()
            val byteArray = response.body()?.bytes()
            if(byteArray != null) {
                return AppResult(byteArray)
            }
        } catch (e: Exception) {
            Logger.e(e)
        }

        return AppResult(Constants.Messages.COMMUNICATION_ERROR)
    }

    private fun <T> getApiResult(
        call: Call<T>,
        clazz: Class<T>? = null,
        whenSuccessResultProducer: ((T) -> AppResult<T>)? = null,
        whenSuccessNullBodyResultProducer: (() -> AppResult<T>)? = null,
        whenErrorResultProducer: ((Int, T?) -> AppResult<T>)? = null): AppResult<T> {
        try {
            val response = call.execute()

            if(response.isSuccessful) {
                val responseBody = response.body()
                if(responseBody != null) {
                    if(whenSuccessResultProducer != null) {
                        return whenSuccessResultProducer.invoke(responseBody)
                    }
                    return AppResult(responseBody)
                }
                if(whenSuccessNullBodyResultProducer != null) {
                    return whenSuccessNullBodyResultProducer.invoke()
                }
            } else {
                if (whenErrorResultProducer != null) {
                    val errorResponseBody = response.errorBody()
                    var errorBody: T? = null
                    if(errorResponseBody != null && clazz != null) {
                        try {
                            val converter = mRetrofit.responseBodyConverter<T>(clazz, emptyArray())
                            errorBody = converter.convert(errorResponseBody)
                        } catch (e: Exception) {
                            Logger.e(e)
                        }
                    }

                    return whenErrorResultProducer.invoke(response.code(), errorBody)
                }
                return getCommonErrorResultByResponseCode(response.code())
            }
        } catch (e: Exception) {
            Logger.e(e)
        }

        return AppResult(Constants.Messages.COMMUNICATION_ERROR)
    }

    private fun <T> getCommonErrorResultByResponseCode(code: Int): AppResult<T> {
        return when(code) {
            500 -> AppResult(Constants.Messages.INTERNAL_SERVER_ERROR)
            else -> AppResult(Constants.Messages.COMMUNICATION_ERROR)
        }
    }
}