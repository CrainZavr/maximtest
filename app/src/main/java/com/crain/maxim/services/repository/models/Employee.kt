package com.crain.maxim.services.repository.models

class Employee (
    id: String,
    name: String,
    val title: String,
    val email: String,
    val phone: String
) : StaffItem(id, name)