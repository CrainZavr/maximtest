package com.crain.maxim.services.api

import android.graphics.Bitmap
import com.crain.maxim.services.api.models.ApiDepartment
import com.crain.maxim.services.api.models.ApiLoginResult
import com.crain.maxim.types.AppResult

interface IApiService {
    fun login(login: String, password: String): AppResult<ApiLoginResult>
    fun getAllDepartments(): AppResult<ApiDepartment>
    fun getEmployeePhoto(employeeId: String): AppResult<ByteArray>
}