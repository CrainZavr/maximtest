package com.crain.maxim.services.api.models

import com.crain.maxim.services.repository.models.Department

class ApiDepartment (
    val ID: String?,
    val Name: String?,
    val Departments: Array<ApiDepartment>?,
    val Employees: Array<ApiEmployee>?
) {
    companion object {
        fun convertToDepartment(apiDepartment: ApiDepartment): Department {
            return Department(
                apiDepartment.ID ?: "",
                apiDepartment.Name ?: "",
                Array(apiDepartment.Departments?.size ?: 0) {
                    convertToDepartment(apiDepartment.Departments!![it])
                },
                Array(apiDepartment.Employees?.size ?: 0) {
                    ApiEmployee.convertToEmployee(apiDepartment.Employees!![it]) }
            )
        }
    }
}