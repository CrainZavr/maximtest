package com.crain.maxim.services.api

import com.crain.maxim.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object RetrofitFactory {
    fun create(baseUrl: String): Retrofit {
        val httpClientBuilder = OkHttpClient.Builder()

        addLogginInterceptor(httpClientBuilder)

        val client = httpClientBuilder.build()

        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(MoshiConverterFactory.create())
            .client(client)
            .build()
    }

    private fun addLogginInterceptor(httpClientBuilder: OkHttpClient.Builder) {
        if (BuildConfig.DEBUG) {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.HEADERS
            httpClientBuilder.addInterceptor(loggingInterceptor)
        }
    }
}