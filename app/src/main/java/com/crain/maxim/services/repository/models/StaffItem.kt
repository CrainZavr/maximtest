package com.crain.maxim.services.repository.models

import java.io.Serializable

abstract class StaffItem (
    val id: String,
    val name: String
) : Serializable