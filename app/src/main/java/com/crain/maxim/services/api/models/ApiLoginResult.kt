package com.crain.maxim.services.api.models

class ApiLoginResult(
    val Message: String = "",
    val Success: Boolean = false
)