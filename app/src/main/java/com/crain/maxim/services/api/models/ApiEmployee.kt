package com.crain.maxim.services.api.models

import com.crain.maxim.services.repository.models.Employee

class ApiEmployee (
    val ID: String?,
    val Name: String?,
    val Title: String?,
    val Email: String?,
    val Phone: String?
) {
    companion object {
        fun convertToEmployee(apiEmployee: ApiEmployee): Employee {
            return Employee(
                apiEmployee.ID ?: "",
                apiEmployee.Name ?: "",
                apiEmployee.Title ?: "",
                apiEmployee.Email ?: "",
                apiEmployee.Phone ?: ""
            )
        }
    }
}