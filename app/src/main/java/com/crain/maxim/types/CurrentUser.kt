package com.crain.maxim.types

import com.crain.maxim.services.preference.IPreferenceService

class CurrentUser(private val mPreferenceService: IPreferenceService) {

    var login: String private set
    var password: String private set

    init {
        this.login = mPreferenceService.currentUserLogin
        this.password = mPreferenceService.currentUserPassword
    }

    val isLogged get() = login.isNotEmpty() and password.isNotEmpty()

    fun login(login: String, password: String) {
        setCredentials(login, password)
    }

    fun logout() {
        setCredentials("", "")
    }

    private fun setCredentials(login: String, password: String) {
        mPreferenceService.currentUserLogin = login
        mPreferenceService.currentUserPassword = password

        this.login = login
        this.password = password
    }
}