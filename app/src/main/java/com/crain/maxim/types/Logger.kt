package com.crain.maxim.types

import android.util.Log

object Logger {

    private const val TAG = "MAXIM"

    fun d(tag: String?, message: String) {
        Log.d("$TAG $tag", message)
    }

    fun d(message: String) {
        d(null, message)
    }

    fun w(message: String) {
        w(null, message)
    }

    fun w(tag: String?, message: String) {
        Log.w("$TAG $tag", message)
    }

    fun e(ex: Throwable) {
        e(null, ex)
    }

    fun e(message: String?, ex: Throwable) {
        Log.e(TAG, message, ex)
    }
}