package com.crain.maxim.types

object Constants {
    object Api {
        const val BASE_URL = "https://contact.taxsee.com/Contacts.svc/"
    }

    object Messages {
        const val COMMUNICATION_ERROR = "Communication error"
        const val INTERNAL_SERVER_ERROR = "Internal server error"
    }
}