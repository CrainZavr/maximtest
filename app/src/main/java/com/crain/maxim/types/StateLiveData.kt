package com.crain.maxim.types

import androidx.lifecycle.MutableLiveData
import java.util.concurrent.atomic.AtomicBoolean

open class StateLiveData<T> : MutableLiveData<T>() {
    private val mCalculated: AtomicBoolean = AtomicBoolean(false)

    val isCalculated: Boolean = mCalculated.get()

    fun checkNotCalculatedAndSet(): Boolean {
        return mCalculated.compareAndSet(false, true)
    }
}